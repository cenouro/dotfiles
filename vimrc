set nocompatible
filetype plugin indent on
syntax on
let g:mapleader = ","

call plug#begin('~/.vim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'kevinoid/vim-jsonc'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'rakr/vim-one'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-sensible'
call plug#end()

autocmd BufWritePre * :%s/\s\+$//e
set backspace=start,eol,indent
set cursorline
set diffopt+=algorithm:patience,indent-heuristic
set encoding=utf-8
set hidden
set list
set modeline
set noswapfile nobackup
set number relativenumber
set tabstop=4 shiftwidth=4 softtabstop=4 expandtab autoindent smartindent
set termguicolors

augroup ft_basic_configs
    autocmd!
    autocmd FileType gitcommit,markdown setlocal spell spelllang=en_us
    " list option generates visual noise in git-related files
    autocmd FileType git,gitcommit,fugitive setlocal nolist
augroup END

let g:lightline = { 'colorscheme': 'one' }
colorscheme one
set background=dark
call one#highlight('SpellBad', '', '', 'underline')
call one#highlight('SpellCap', '', '', 'underline')
call one#highlight('SpellLocal', '', '', 'underline')
call one#highlight('SpellRare', '', '', 'underline')
" Tweak colors used by 'set list' chars. See :h listchars
call one#highlight('SpecialKey', 'be5046', 'd19a66', '')

